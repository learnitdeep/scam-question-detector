#!/usr/bin/python
import lex.demo
import bottle
from bottle import static_file, route, request, run, post, get, response

WEB_PATH = 'web/'
LEXICON_PATH = 'data/lexicons/paralex'
WEIGHTS_PATH = 'data/weights/paralex.txt'
DATABASE_PATH = 'data/database'
NLP_PORT = 8082
NLP_HOST = 'localhost'

qa = lex.demo.QA(lexicon_path=LEXICON_PATH, weights_path=WEIGHTS_PATH, 
    database_path=DATABASE_PATH, nlp_port=NLP_PORT, nlp_host=NLP_HOST)

@get('/parse')
def parse():
    '''
    Web method that parses an input sentence. Expects the input sentence to
    be stored in a url parameter "text". Returns a JSON object with keys
    "result", "status", and "message".
    '''
    result = dict()
    try:
        sent = request.GET.get('sent')
        if not sent:
            raise ValueError('sent required')
        result['result'] = qa.answer_obj(sent)
    except Exception, e:
        result['status'] = 'error'
        result['message'] = str(e.message)
    return result

@route('/')
def index():
    return static_file('index.html', root=WEB_PATH)

@route('/<filepath:path>')
def server_static(filepath):
    return static_file(filepath, root=WEB_PATH)

if __name__ == '__main__': 
    bottle.BaseRequest.MEMFILE_MAX = 50000000
    run(host='0.0.0.0', port=8083)
