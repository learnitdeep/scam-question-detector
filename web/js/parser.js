var initializeParser = function() {
    $('#inputbox').keypress(function(e) {
        code = (e.keyCode ? e.keyCode : e.which);
        if (code == 13) {
            e.preventDefault();
            parse();
        }
    });
    $('#inputbox').focus();
};

var showParseResults = function($r, result) {
    console.log(result);
    var $table = $('<table border=1 />');
    $.each(result, function(i, obj) {
        $table.append(makeRow(obj));
    });
    $r.append($table);
    prettyPrint();
};

var makeRow = function(obj) {
    var $row = $('<tr/>');
    $row.append('<td valign=top>' + obj.score.toFixed(2) + '</td>');
    $ans = $('<ul/>');
    $.each(obj.answers, function(i, answer){ 
        $ans.append('<li>'+answer+'</li>');
    });
    var $info = $('<td valign=top/>').appendTo($row);
    $info.append($('<pre class="prettyprint lang-sql"/>').append(obj.query).appendTo($row));
    var $means = $('<table border=1 width=400/>').appendTo($info);
    $means.append('<tr><th>String</th><th>Interpretation</th></tr>');
    $.each(obj.parse, function(i, pair) {
        var $sr = $('<tr/>').appendTo($means)
        var s = pair[0];
        var m = pair[1];
        $sr.append('<td>'+s+'</td>');
        $sr.append('<td>'+m+'</td>');
    });
    $means.append('<br/>');

    console.log(obj);
    if (obj.answers.length > 0) {
        $row.append($('<td valign=top/>').append($ans));
    } else {
        $row.append('<td valign=top></td>');
    }
    return $row;
};

var parserUrl = '/parse?';
var parse = function() {
    $('#results').html('<img src="spinner.gif"/>');
    var sent = $('#inputbox').val();
    $.getJSON(parserUrl, {'sent': sent}, function(data) {
        $('#results').html('');
        if (data.status == 'error') {
            showError(data.message);
        } else {
            showParseResults($('#results'), data.result);
        }
    })
    .error(function() {
        showError('Could not contact server.');
    });
};

var showError = function(err) {
    $('#results').html('');
    var $results = $('#results');
    $results.append('<div class="error">' + err + '</div>');
};

function getURLParameter(name) {
    return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null;
}
