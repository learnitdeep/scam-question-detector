import qa.nlp as nlp

def get_sent(text, **kwargs):
    nlp_host = kwargs['nlp_host']
    nlp_port = kwargs['nlp_port']
    try:
        sents = nlp.process(text, hostname=nlp_host, port=nlp_port)
    except Exception, e:
        msg = str(e)
        raise IOError('Could not pre-process sentence on %s:%s: %s' 
            % (nlp_host, nlp_port, msg))

    #if len(sents) != 1:
    #    raise ValueError('invalid number of sentences: %s => %s' % (text, sents))
    #print str(sents)
    sent = sents[0]
    return sent
