import re
import qa.jsonrpc as jsonrpc
from simplejson import loads

def get_server(hostname='localhost', port=8080, timeout=10.0):
    version = jsonrpc.JsonRpc20()
    transport = jsonrpc.TransportTcpIp(addr=(hostname, port), timeout=timeout)
    server = jsonrpc.ServerProxy(version, transport)
    return server

def process(s, hostname='localhost', port=8080, timeout=10.0):
    server = get_server(hostname, port, timeout)
    results = loads(server.parse(s))
    print str(results)
    sents = []
    for sent in results['sentences']:
        words_obj = sent['words']
        word = [word_obj[0] for word_obj in words_obj]
        pos = [word_obj[1]['PartOfSpeech'] for word_obj in words_obj]
        lemma = [word_obj[1]['Lemma'] for word_obj in words_obj]
        ner = [word_obj[1]['NamedEntityTag'] for word_obj in words_obj]
        sents.append(dict(word=word, pos=pos, lemma=lemma, ner=ner))
    return sents
