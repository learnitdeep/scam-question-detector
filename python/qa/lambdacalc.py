import re
'''
This is a bunch of code for manipulating lisp expressions and doing basic
lambda calculus in python tuples.
'''

# These are functions that bind a new variable.
VARIABLE_BINDING = ['lambda', 'exists']

expr = tuple

def unk_type_err(y=None):
    raise ValueError('Unknown type: %r' % y)

def is_atom(y):
    return type(y) == str or type(y) == unicode

def is_complex(y):
    return type(y) == tuple

def fapply(x, y):
    '''
    Equivalent to application(x, y)
    '''
    return application(x, y)

def bapply(x, y):
    '''
    Equivalent to application(y, x)
    '''
    return application(y, x)

def fcomp(x, y):
    '''
    Equivalent to composition(x, y)
    '''
    return composition(x, y)

def bcomp(x, y):
    '''
    Equivalent to composition(y, x)
    '''
    return composition(y, x)

def composition(x, y):
    '''
    Returns the function composition (lambda $v (x (y $v))). Beta-reduces
    the result and renumbers the variables.
    '''
    x = normalize_expression(x)
    y = normalize_expression(y)
    new_expr = normalize_expression(('lambda', '$v', (x, (y, '$v'))))
    return normalize_expression(new_expr)

def application(x, y):
    '''
    Returns the function application (x y). Beta-reduces and renumbers the
    variables.
    '''
    app = normalize_expression((x, y))
    return normalize_expression(app)

def normalize_expression(y, comm_fns=['and', 'or'], assoc_fns=['and', 'or']):
    '''
    Renumbers the bound variables in y, applies beta reduction, sorts the
    arguments of commutative functions, and flattens nested calls to 
    associative functions.
    '''
    y = renumber_bound_variables(y)
    y = beta_reduce(y)
    for assoc_fn in assoc_fns: y = flatten_assoc(y, assoc_fn)
    for comm_fn in comm_fns: y = sort_fn_args(y, comm_fn)
    y = renumber_bound_variables(y)
    y = beta_reduce(y)
    return y

def sort_fn_args(y, fn_name):
    '''
    Sorts the arguments of each function application with name fn_name.
    This is for normalizing commutative functions.
    '''
    if is_atom(y):
        return y
    elif is_complex(y):
        if y[0] == fn_name:
            sorted_children = sorted(sort_fn_args(x, fn_name) for x in y[1:])
            return expr((fn_name,)) + expr(sorted_children)
        else:
            return expr(sort_fn_args(x, fn_name) for x in y)
    else:
        unk_type_err(y)

def flatten_assoc(y, fn_name):
    '''
    Takes an expression y and looks for nested applications of the function
    with functio name fn_name. Flattens them into a single call of the 
    function. This is for normalizing associative functions.
    '''
    if is_atom(y):
        return y
    elif is_complex(y):
        name = y[0]
        if name == fn_name:
            flat_children = expr(flatten_assoc(x, fn_name) for x in y[1:])
            new_children = []
            for child in flat_children:
                if is_complex(child) and child[0] == fn_name:
                    new_children.extend(child[1:])
                else:
                    new_children.append(child)
            return expr((fn_name,)) + expr(new_children)
        else:
            return expr(flatten_assoc(x, fn_name) for x in y)
    else:
        unk_type_err(y)

def renumber_bound_variables(y):
    '''
    Returns y, but with the bound variables renumbered to be $0, $1, ...
    '''
    k, result = renumber_bound_variables_helper(y)
    return result

def renumber_bound_variables_helper(y, env=None, k=0):
    '''
    Returns the renumbered expression and the next variable number.
    '''
    if env is None: env = dict()
    if is_atom(y):
        new_y = y
        if y in env:
            new_y = env[y]
        return k, new_y
    elif is_complex(y):
        old_var = None
        if is_variable_binding(y):
            old_var = formal_var(y)
            new_var = '$%s' % k
            env[old_var] = new_var
            k += 1
        new_parts = []
        for x in y:
            k, new_x = renumber_bound_variables_helper(x, env, k)
            new_parts.append(new_x)
        new_y = expr(new_parts)
        if old_var is not None:
            if old_var in env:
                env.pop(old_var)
            else:
                raise ValueError("Expected %s, maybe defined twice in %s?" % (old_var, lisp2str(y)))
        return k, new_y
    else:
        unk_type_err(y)

def beta_reduce(y):
    '''
    Applies beta-reduction recursively to y by first beta-reducing y, then
    beta-reducing all sub-expressions.
    '''
    if is_atom(y):
        return y
    elif is_complex(y):
        narg = len(y)
        if narg > 1 and is_lambda(y[0]):
            lambda_expr = y[0]
            var = formal_var(lambda_expr)
            body = lambda_body(lambda_expr)
            arg = y[1]
            new_body = substitute(body, var, arg)
            if narg == 2 and is_complex(new_body):
                return expr(beta_reduce(x) for x in new_body)
            elif narg > 2:
                return expr(beta_reduce((new_body,) + y[2:]))
            else:
                return new_body
        else:
            return expr(beta_reduce(x) for x in y)
    else:
        unk_type_err(y)

def substitute(y, before, after):
    '''
    Replaces all occurrences of before with after in y.
    '''
    return substitute_map(y, {before: after})

def substitute_map(y, m):
    '''
    Takes a dictionary m mapping keys to values. Replaces all keys in y with
    values in y.
    '''
    if y in m:
        return m[y]
    elif is_atom(y):
        return y
    elif is_complex(y):
        return expr(substitute_map(x, m) for x in y)
    else:
        unk_type_err(y)

def bound_vars(y):
    '''
    Returns a list of bound variables in y.
    '''
    vs = set()
    for x in subexpr_iter(y):
        if is_variable_binding(x):
            v = formal_var(x)
            vs.add(v)
    return vs

def is_variable_binding(y):
    '''
    Returns true if the given expression introduces a new bound variable
    into the scope.
    '''
    return is_complex(y) and y[0] in VARIABLE_BINDING

def is_lambda(y):
    '''
    Returns True if this is a lambda expression. A lambda expression has the 
    form (lambda var body) where var is a formal variable name (starting 
    with $) and body is some lisp 
    expression.
    '''
    return is_complex(y) \
        and len(y) == 3 \
        and y[0] == 'lambda' \
        and y[1].startswith('$')

def var_bind_err(y=None):
    raise ValueError('Expected var binding expression, got %r' % y)

def formal_var(y):
    '''
    Returns the formal variable of the given variable binding expression. 
    '''
    if is_variable_binding(y):
        return y[1]
    else:
        var_bind_err(y)

def lambda_body(y):
    '''
    Returns the body of the given lambda expression.
    '''
    if is_lambda(y):
        return y[2]
    else:
        var_bind_err(y)

def term_iter(y):
    '''
    An iterator over all terms (atoms) in y
    '''
    for subexpr in subexpr_iter(y):
        if is_atom(subexpr):
            yield subexpr

def subexpr_iter(y):
    '''
    An iterator over all subexpressions of y (including y itself)
    '''
    if is_atom(y):
        yield y
    elif is_complex(y):
        for x in y:
            for z in subexpr_iter(x):
                yield z
        yield y
    else:
        unk_type_err(y)

def lisp2str(y):
    '''
    Converts a nested tuple representation of a lisp expression to a string.
    '''
    if y is None:
        return y
    else:
        return to_string(y)

comment = re.compile(r'//.*\n')
def str2lisp(s):
    '''
    Converts a string into a nested tuple representing a lisp expression.
    '''
    return deep_tuple(parse(comment.sub(' ', s)))

def deep_tuple(l):
    '''
    Convert a nested list object to a nested tuple.
    '''
    if type(l) == list:
        return tuple(deep_tuple(x) for x in l)
    elif is_atom(l):
        return l
    else:
        raise ValueError('Unexpected type: %s' % l)

def lambda_equals(x, y):
    '''
    Checks to see if x and y are equal lambda calculus expressions. Does this
    by running normalize_expression(x) == normalize_expression(y)
    '''
    x = normalize_expression(x)
    y = normalize_expression(y)
    return lambda_equals_helper(x, y)

def lambda_equals_helper(x, y):
    '''
    '''
    leh = lambda_equals_helper
    if is_atom(x) and is_atom(y):
        return x == y
    elif is_lambda(x) and is_lambda(y):
        return leh(x[2:], y[2:])
    elif not is_lambda(x) and not is_lambda(y):
        return len(x) == len(y) and all(leh(a, b) for (a,b) in zip(x,y))
    else:
        return False

'''
The code below is from Peter Norvig.
http://norvig.com/lispy.html
'''

def parse(s):
    "Read a Scheme expression from a string."
    try:
        return read_from(tokenize(s))
    except Exception, e:
        raise ValueError("Could not parse %s: %s" % (s, str(e)))

def tokenize(s):
    "Convert a string into a list of tokens."
    return s.replace('(',' ( ').replace(')',' ) ').split()

def read_from(tokens):
    "Read an expression from a sequence of tokens."
    if len(tokens) == 0:
        raise SyntaxError('unexpected EOF while reading')
    token = tokens.pop(0)
    if '(' == token:
        L = []
        while tokens[0] != ')':
            L.append(read_from(tokens))
        tokens.pop(0) # pop off ')'
        return L
    elif ')' == token:
        raise SyntaxError('unexpected )')
    else:
        return token

def to_string(exp):
    "Convert a Python object back into a Lisp-readable string."
    return '('+' '.join(map(to_string, exp))+')' \
        if isinstance(exp, tuple) else str(exp)

def extract_args(exp):
    '''
    Extracts the list of formal lambda arguments of the expression, and
    its body. For example, if the expression is 

        (lambda $x (lambda $y (lambda $f (($f $x) $y))))
        
    this function will return a list of the variable names $x, $y, $f, and
    the body (($f $x) $y).
    '''
    if is_lambda(exp):
        fvar = formal_var(exp)
        lbody = lambda_body(exp)
        fvars, body = extract_args(lbody)
        return ([fvar] + fvars, body)
    else:
        return ([], exp)
