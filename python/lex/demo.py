import lex.lexicon
import lex.semantics as sem
import lex.vocab
import lex.lambdacalcutil as lcu
import lex.learn
import lex.parse
import lex.db
import qa.server

class QA:

    def __init__(self, lexicon_path='data/lexicons/paralex_iter1', weights_path='data/weights/paralex_iter1/mixed.iter21', database_path='data/database', nlp_port=8082, nlp_host='rv-n15'):
        nl_vocab = lex.vocab.read_vocab(open('%s/vocab.txt' % lexicon_path))
        nl_vocab_inv = lex.vocab.read_vocab_inv(open('%s/vocab.txt' % lexicon_path))
        db_vocab = lex.vocab.read_vocab(open('%s/vocab.txt' % database_path))
        db_vocab_inv = lex.vocab.read_vocab_inv(open('%s/vocab.txt' % database_path))
        lexicon = lex.lexicon.read_lexicon(open('%s/lexicon.txt' % lexicon_path))
        weights = lex.learn.load_weights(open(weights_path))
        db = lex.db.open_db('%s/newtuples.db' % database_path)
        config = dict(nlp_port=nlp_port, nlp_host=nlp_host)
        get_sent = lambda x: qa.server.get_sent(x, **config)

        self.get_sent = get_sent
        self.nl_vocab = nl_vocab
        self.nl_vocab_inv = nl_vocab_inv
        self.lexicon = lexicon
        self.weights = weights
        self.db_vocab = db_vocab
        self.db_vocab_inv = db_vocab_inv
        self.db = db

    def answer(self, q):
        sent = self.get_sent(q)
        scored_queries = lex.parse.get_scored_queries_fast(sent, self.nl_vocab, self.lexicon, self.weights)
        for score, query in scored_queries[0:100]:
            answers = lex.db.query(query, self.db, self.db_vocab, self.db_vocab_inv)
            for a in answers:
                full_a = lex.semantics.apply(query, a)
                sa = (
                    self.db_vocab_inv[sem.rel0_rel_of(full_a)],
                    self.db_vocab_inv[sem.rel0_arg1_of(full_a)],
                    self.db_vocab_inv[sem.rel0_arg2_of(full_a)],
                )
                yield score, sa

    def answer_obj(self, q):
        if not q.endswith('?'): q = q + '?'
        sent = self.get_sent(q)


	scored_queries = lex.parse.get_scored_queries_fast2(sent, self.nl_vocab, self.lexicon, self.weights)
        results = []
        for score, parse, query in scored_queries[0:100]:
            obj = dict(score=score, answers=[])
            answers = lex.db.query(query, self.db, self.db_vocab, self.db_vocab_inv)

            q = lex.db.query2sql(query, self.db_vocab, self.db_vocab_inv)
            print q
	    s = q[0]
            for x in q[1]:
                s = s.replace('?', '"%s"' % x, 1)
            obj['query'] = s

            obj['parse'] = [
                (pstr.string, lcu.sem2str(ss, self.db_vocab_inv)) for (pstr, ss) in zip(parse.strings, parse.concepts)
            ]

            for a in answers:
                full_a = lex.semantics.apply(query, a)
                sa = (
                    self.db_vocab_inv[sem.rel0_rel_of(full_a)],
                    self.db_vocab_inv[sem.rel0_arg1_of(full_a)],
                    self.db_vocab_inv[sem.rel0_arg2_of(full_a)],
                )
                obj['answers'].append(self.db_vocab_inv[a[1]])
            results.append(obj)
        return results

    def queries(self, q):
        sent = self.get_sent(q)
        scored_queries = lex.parse.get_scored_queries_fast(sent, self.nl_vocab, self.lexicon, self.weights)
        for score, query in scored_queries[0:15]:
            yield lex.db.query2sql(query, self.db_vocab, self.db_vocab_inv)
