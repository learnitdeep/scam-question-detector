from time import time
import lex.lexicon
import lex.semantics as sem
import lex.vocab
import lex.learn
import lex.parse
import sys

lexicon_path = sys.argv[1]
weights_path = sys.argv[2]
nl_vocab = lex.vocab.read_vocab(open('%s/vocab.txt' % lexicon_path))        
lexicon = lex.lexicon.read_lexicon(open('%s/lexicon.txt' % lexicon_path))
weights = lex.learn.load_weights(open(weights_path))
t00 = time()
for line in sys.stdin:
    t0 = time()
    sent = dict(lemma=line.strip().split(' '))
    scored_queries = lex.parse.get_scored_queries_fast(sent, nl_vocab, lexicon, weights)
    print time() - t0
print 'total',time()-t00
