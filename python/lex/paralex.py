from time import time
import lex.parse
import lex.align
import lex.vocab
import lex.lexicon
import lex.learn
import lex.semantics as sem
import sys
patterns_out = open(sys.argv[3], 'w')
labels_out = open(sys.argv[4], 'w')
path = sys.argv[1]
print >>sys.stderr, 'loading vocab'
nl_vocab = lex.vocab.read_vocab(open('%s/vocab.txt' % path))
print >>sys.stderr, 'loading lexicon'
lexicon = lex.lexicon.read_lexicon(open('%s/lexicon.txt' % path))
print >>sys.stderr, 'loading weights'
weights = lex.learn.load_weights(open(sys.argv[2]))
print >>sys.stderr, 'done'

maxlen = 15
t00 = time()
for i, line in enumerate(sys.stdin):
    t0 = time()
    if i % 100000 == 0: print >>sys.stderr, i
    try:
        q1, q2, alignment = lex.align.read_alignment_line(line)        
        if len(q1['lemma']) > maxlen: continue
        if len(q2['lemma']) > maxlen: continue
        sent = ' '.join(q2['lemma'])
        for string, meaning in lex.align.paralex_best(q1, q2, alignment, nl_vocab, lexicon, weights):
            print >>patterns_out, '%s\t%s\t%s' % (sent, string.string, sem.obj2str(meaning))
        queries = lex.parse.get_scored_queries_fast(q1, nl_vocab, lexicon, weights)
        if queries:
            print >>labels_out, '%s\t%s' % (sent, sem.obj2str(queries[0][1]))
        print >>sys.stderr, 'time',time()-t0
    except:
        print >>sys.stderr, 'could not deal with: %r' % line
        continue
patterns_out.close()
labels_out.close()
print >>sys.stderr, 'rows', i
print >>sys.stderr, 'final time', time()-t00

##i = 0
#for sent, s, m in paralex_input_best(sys.stdin, nl_vocab, lexicon, weights, maxlen=15, maxnllen=10):
##    i += 1
##    if i > 10000: break
#    print '%s\t%s\t%s' % (sent, s, sem.obj2str(m))
