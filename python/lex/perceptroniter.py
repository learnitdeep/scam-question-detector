import lex.learn as learn
import lex.lexicon as lexicon
import lex.vocab as vocab
import sys
import itertools
lexicon_path = sys.argv[1]
weight_path = sys.argv[2]
example_path = sys.argv[3]
output_path = sys.argv[4]

learn.log('loading %s/lexicon.txt' % lexicon_path)
lex = lexicon.read_lexicon(open(lexicon_path + '/lexicon.txt'))
learn.log('loading %s/vocab.txt' % lexicon_path)
nl_vocab = vocab.read_vocab(open(lexicon_path + '/vocab.txt'))
learn.log('loading %s' % weight_path)
weights = learn.load_weights(open(weight_path))
examples = itertools.islice(learn.example_iter(example_path), 100000)
learn.perceptron_epoch(examples, nl_vocab, lex, weights)
learn.log('saving to %s' % output_path)
learn.save_weights(open(output_path, 'w'), weights)
