import sys
from collections import defaultdict
from lex.semantics import str2obj as str2semantics

def swap(a, b):
    return b, a

def ident(a, b):
    return a, b

def read_vocab(input, fn=ident):
    result = dict()
    for i, line in enumerate(input):
        if i % 100000 == 0: print >>sys.stderr, i
        id, word = line.strip().replace('\t', ' ').split(' ', 1)
        id = int(id)
        word = word.strip()
        key, value = fn(word, id)
        result[key] = value
    return result

def read_vocab_inv(input):
    return read_vocab(input, fn=swap)
