import sys
from lex.semantics import str2obj as str2semantics
from collections import defaultdict
def read_lexicon(input, k=float('inf')):
    lexicon = defaultdict(list)
    for i, line in enumerate(input):
        if i >= k: break
        if i % 100000 == 0: print >>sys.stderr, i
        try:
            nl, dbs = line.split(' ', 1)
            nl = int(nl)
            dbs = str2semantics(dbs)
            lexicon[nl].append(dbs)
        except:
            print >>sys.stderr, 'could not parse line %r' % line
            continue
    return lexicon
