ENT = 0
ABSTR = 1
REL1 = 2
REL2 = 3
REL0 = 4

ORDER_12 = 0
ORDER_21 = 1

def type_of(x): 
    return x[0]

def arg_order_of(x):
    return x[1]

def rel2_dbid_of(x):
    return x[2]

def ent_dbid_of(x):
    return x[1]

def rel1_ent_dbid_of(x):
    return x[3]

def rel1_rel_dbid_of(x):
    return x[2]

def rel0_arg1_of(x): return x[2]
def rel0_arg2_of(x): return x[3]
def rel0_rel_of(x): return x[1]

def new_ent(dbid):
    return (ENT, dbid)

def new_rel0(rel_dbid, arg1_dbid, arg2_dbid):
    return (REL0, rel_dbid, arg1_dbid, arg2_dbid)

def new_abstr(arg_order):
    if arg_order != ORDER_12 and arg_order != ORDER_21:
        raise ValueError('unknown arg order: %s' % arg_order)
    return (ABSTR, arg_order)

def new_rel2(arg_order, dbid):
    if arg_order != ORDER_12 and arg_order != ORDER_21:
        raise ValueError('unknown arg order: %s' % arg_order)
    return (REL2, arg_order, dbid)

def new_rel1(arg_order, rel_dbid, ent_dbid):
    if arg_order != ORDER_12 and arg_order != ORDER_21:
        raise ValueError('unknown arg order: %s' % arg_order)
    return (REL1, arg_order, rel_dbid, ent_dbid)

def apply_abstr(abstr, rel):
    ao = arg_order_of(abstr)
    ro = arg_order_of(rel)
    no = None
    if ao == ORDER_12 and ro == ORDER_12:
        no = ORDER_21
    elif ao == ORDER_12 and ro == ORDER_21:
        no = ORDER_12
    elif ao == ORDER_21 and ro == ORDER_12:
        no = ORDER_12
    elif ao == ORDER_21 and ro == ORDER_21:
        no = ORDER_21
    else:
        raise ValueError('unexpected orders: %s and %s' % (ao, ro))
    return new_rel2(no, rel2_dbid_of(rel))

def apply_rel2(rel2, ent):
    o = arg_order_of(rel2)
    rdbid = rel2_dbid_of(rel2)
    edbid = ent_dbid_of(ent)
    return new_rel1(o, rdbid, edbid)

def apply_rel1(rel1, ent):
    relid = rel1_rel_dbid_of(rel1)
    constid = rel1_ent_dbid_of(rel1)
    entid = ent_dbid_of(ent)
    o = arg_order_of(rel1)
    if o == ORDER_12:
        return new_rel0(relid, constid, entid)
    elif o == ORDER_21:
        return new_rel0(relid, entid, constid)
    else:
        raise ValueError('unexpected orders: %s and %s' % (ao, ro))

def apply(x, y):
    tx = type_of(x)
    ty = type_of(y)
    if tx == ABSTR and ty == REL2:
        return apply_abstr(x, y)
    elif tx == REL2 and ty == ENT:
        return apply_rel2(x, y)
    elif tx == REL1 and ty == ENT:
        return apply_rel1(x, y)
    else:
        raise ValueError('Cannot apply types %s, %s' % (tx, ty))

def str2obj(s):
    fields = list(int(x) for x in s.split())
    t = fields.pop(0)
    f = None
    if t == ENT: f = new_ent
    elif t == ABSTR: f = new_abstr
    elif t == REL2: f = new_rel2
    elif t == REL1: f = new_rel1
    else: raise ValueError('Unknown semantic type: %s' % t)
    return f(*fields)

def obj2str(x):
    return ' '.join(str(v) for v in x)
