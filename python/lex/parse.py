import lex.semantics as sem
import time
from math import exp
from math import log
import sys
import heapq
import re
from collections import namedtuple
from collections import defaultdict
from itertools import product
from itertools import chain
import random
Parse = namedtuple('Parse', 'sent meaning strings concepts features')
Substring = namedtuple('Substring', 'string indexes')

stops = {'the', 'a', 'be', 'what', 'which', 'in', 'of', '?', 'do', 'where',
    'how', 'when', 'why', 'and', 'for', 'who', 'you', 'to', 'use', 'make',
    'can', 'from', 'name', "'s", 'on', 'have', 'that', 'many', 'most', 'by',
    'they', 'he', 'she', 'at', 'with', 'or', 'as'}
pos_stops = {'IN', 'DT', 'PRP'}

def generate_subs(n, maxlen=None):
    if maxlen is None: maxlen = n
    for i in xrange(n):
        for j in xrange(i, n):
            if j - i >= n: break
            yield (i, j)

def generate_subs_pair(n, maxlen=None):
    for a, b in generate_subs(n, maxlen):
        for c, d in generate_subs(n-b, maxlen):
            yield a, b, b+c, b+d

def trange(*args):
    return tuple(range(*args))

def generate_patterns_pair(sent, maxlen=5):
    lem = [x.lower() for x in sent['lemma']]
    n = len(lem)
    for a,b,c,d in generate_subs_pair(n, maxlen):
        qpat = ' '.join(lem[0:a] + ['$r'] + lem[b:c] + ['$y'] + lem[d:])
        rpat = ' '.join(lem[a:b])
        ypat = ' '.join(lem[c:d])
        if all(x in stops for x in lem[a:b]): continue
        if all(x in stops for x in lem[c:d]): continue
        if qpat and rpat and ypat and ypat not in stops and rpat not in stops:
            yield Substring(qpat, trange(a) + trange(b, c) + trange(d, n)), Substring(rpat, trange(a,b)), Substring(ypat, trange(c,d))
        qpat = ' '.join(lem[0:a] + ['$y'] + lem[b:c] + ['$r'] + lem[d:])
        ypat = ' '.join(lem[a:b])
        rpat = ' '.join(lem[c:d])
        if qpat and rpat and ypat and ypat not in stops and rpat not in stops:
            yield Substring(qpat, trange(a) + trange(b, c) + trange(d, n)), Substring(rpat, trange(c,d)), Substring(ypat, trange(a,b))

def generate_patterns_single(sent, maxlen=5):
    lem = [x.lower() for x in sent['lemma']]
    n = len(lem)
    for a,b in generate_subs(n, maxlen):
        rpat = ' '.join(lem[0:a] + ['$y'] + lem[b:])
        ypat = ' ' .join(lem[a:b])
        if rpat and ypat and ypat not in stops and rpat not in stops:
            yield Substring(rpat, trange(0,a)+trange(b,n)), Substring(ypat, trange(a,b))

def parse_single(sent, nl_vocab, lexicon):
    for rpat, ypat in generate_patterns_single(sent):
        if rpat.string not in nl_vocab: continue
        if ypat.string not in nl_vocab: continue
        r = nl_vocab[rpat.string]
        y = nl_vocab[ypat.string]
        if r not in lexicon: continue
        if y not in lexicon: continue
        rmean = lexicon[r]
        ymean = lexicon[y]
        for rs, ys in product(rmean, ymean):
            if sem.type_of(rs) != sem.REL2: continue
            if sem.type_of(ys) != sem.ENT: continue
            meaning = sem.apply(rs, ys)
            features = [ '%s %s' % (r,' '.join(str(i) for i in rs)), 
                         '%s %s' % (y,' '.join(str(i) for i in ys))]
            yield Parse(sent, meaning, [rpat, ypat], [rs, ys], features)

def parse_pair(sent, nl_vocab, lexicon):
    for qpat, rpat, ypat in generate_patterns_pair(sent):
        if qpat.string not in nl_vocab: continue
        if rpat.string not in nl_vocab: continue
        if ypat.string not in nl_vocab: continue
        r = nl_vocab[rpat[0]]
        y = nl_vocab[ypat[0]]
        q = nl_vocab[qpat[0]]
        if r not in lexicon: continue
        if y not in lexicon: continue
        if q not in lexicon: continue
        qmean = [m for m in lexicon[q] if sem.type_of(m) == sem.ABSTR]
        rmean = [m for m in lexicon[r] if sem.type_of(m) == sem.REL2]
        ymean = [m for m in lexicon[y] if sem.type_of(m) == sem.ENT]
        for qs, rs, ys in product(qmean, rmean, ymean):
            meaning = sem.apply(sem.apply(qs, rs), ys)
            features = [ '%s %s' % (r,' '.join(str(i) for i in rs)), 
                         '%s %s' % (y,' '.join(str(i) for i in ys)),
                         '%s %s' % (q,' '.join(str(i) for i in qs))]
            p = Parse(sent, meaning, [qpat, rpat, ypat], [qs, rs, ys], features)
            yield p

def parse_pair_fast(sent, nl_vocab, lexicon, weights, qk=10, rk=10, yk=10):
    qpat_to_r = defaultdict(list)
    qpat_to_y = defaultdict(list)
    for qpat, rpat, ypat in generate_patterns_pair(sent):
        if qpat.string not in nl_vocab: continue
        if rpat.string not in nl_vocab: continue
        if ypat.string not in nl_vocab: continue
        r = nl_vocab[rpat[0]]
        y = nl_vocab[ypat[0]]
        q = nl_vocab[qpat[0]]
        if r not in lexicon: continue
        if y not in lexicon: continue
        if q not in lexicon: continue
        qpat_to_r[qpat, q].append((rpat, r))
        qpat_to_y[qpat, q].append((ypat, y))

    scored_qs = []
    for qpat, q in qpat_to_r:
        qmean = [m for m in lexicon[q] if sem.type_of(m) == sem.ABSTR]
        for qs in qmean:
            f = '%s %s' % (q, ' '.join(str(i) for i in qs))
            score = weights[f] if f in weights else 0.0
            heapq.heappush(scored_qs, (score, qpat, q, qs))
            if len(scored_qs) > qk:
                heapq.heappop(scored_qs)

    for score, qpat, q, qs in scored_qs:
        
        scored_rs = []
        for rpat, r in qpat_to_r[qpat, q]:
            rmean = [m for m in lexicon[r] if sem.type_of(m) == sem.REL2]
            for rs in rmean:
                f = '%s %s' % (r, ' '.join(str(i) for i in rs))
                score = weights[f] if f in weights else 0.0
                heapq.heappush(scored_rs, (score, rpat, r, rs))
                if len(scored_rs) > rk:
                    heapq.heappop(scored_rs)

        scored_ys = []
        for ypat, y in qpat_to_y[qpat, q]:
            ymean = [m for m in lexicon[y] if sem.type_of(m) == sem.ENT]
            for ys in ymean:
                f = '%s %s' % (y, ' '.join(str(i) for i in ys))
                score = weights[f] if f in weights else 0.0
                heapq.heappush(scored_ys, (score, ypat, y, ys))
                if len(scored_ys) > yk:
                    heapq.heappop(scored_ys)

        count = 0
        for s1, rpat, r, rs in scored_rs:
            for s2, ypat, y, ys in scored_ys:
                count += 1
                meaning = sem.apply(sem.apply(qs, rs), ys)
                features = [ '%s %s' % (r,' '.join(str(i) for i in rs)), 
                             '%s %s' % (y,' '.join(str(i) for i in ys)),
                             '%s %s' % (q,' '.join(str(i) for i in qs))]
                p = Parse(sent, meaning, [qpat, rpat, ypat], [qs, rs, ys], features)
                yield p

def parse(sent, nl_vocab, lexicon):
    return chain(parse_single(sent, nl_vocab, lexicon), 
        parse_pair(sent, nl_vocab, lexicon))

def parse_fast(sent, nl_vocab, lexicon, weights):
    return chain(
        parse_single(sent, nl_vocab, lexicon),
        parse_pair_fast(sent, nl_vocab, lexicon, weights)
    )

def score_parse(p, weights):
    total = 0.0
    for f in p.features:
        if f in weights:
            total += weights[f]
    return total

def get_scored_queries(sent, nl_vocab, lexicon, weights):
    parses = parse(sent, nl_vocab, lexicon)
    scored = list((score_parse(p, weights), p) for p in parses)
    best_scores = defaultdict(lambda: float('-inf'))
    for s, p in scored:
        best_scores[p.meaning] = max(best_scores[p.meaning], s)
    results = list((v,k) for (k,v) in best_scores.iteritems())
    results.sort(reverse=True)
    return results

def get_scored_queries_fast(sent, nl_vocab, lexicon, weights):
    parses = parse_fast(sent, nl_vocab, lexicon, weights)
    scored = list((score_parse(p, weights), p) for p in parses)
    best_scores = defaultdict(lambda: float('-inf'))
    for s, p in scored:
        best_scores[p.meaning] = max(best_scores[p.meaning], s)
    results = list((v,k) for (k,v) in best_scores.iteritems())
    results.sort(reverse=True)
    return results

def get_scored_queries_fast2(sent, nl_vocab, lexicon, weights):
    parses = parse_fast(sent, nl_vocab, lexicon, weights)
    scored = list((score_parse(p, weights), p) for p in parses)
    best_scores = defaultdict(lambda: float('-inf'))
    best_parses = dict()
    for s, p in scored:
        if s > best_scores[p.meaning]:
            best_scores[p.meaning] = s
            best_parses[p.meaning] = p 

    results = []
    for m in best_scores:
        results.append( (best_scores[m], best_parses[m], m) )

#    results = list((v,k) for (k,v) in best_scores.iteritems())
    results.sort(reverse=True)
    return results
