import sys
import lex.vocab
vocab = lex.vocab.read_vocab(open(sys.argv[1]))
for line in sys.stdin:
    try:
        k,v = line.strip().split('\t', 1)
        i = vocab[k]
        print '%s %s' % (i, v)
    except Exception, e:
        print >>sys.stderr, 'could not read %r' % line
        print >>sys.stderr, e
