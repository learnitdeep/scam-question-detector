import sys
import lex.lambdacalcutil as lcu
import lex.vocab
import lex.semantics
import qa.lambdacalc as lc

def weight2str(line, nlv, dbv):
    fields = list(int(x) for x in line.split(' '))
    nl = nlv[fields.pop(0)]
    db = lcu.sem2lisp(tuple(fields), dbv)
    return nl, db

if __name__ == '__main__':
    nl_vocab_inv = lex.vocab.read_vocab_inv(open(sys.argv[1]))
    db_vocab_inv = lex.vocab.read_vocab_inv(open(sys.argv[2]))
    for line in sys.stdin:
        try:
            nl, rest = line.strip().split(' ', 1)
            db = lcu.sem2lisp(lex.semantics.str2obj(rest), db_vocab_inv)
            print '%s\t%s' %(nl, lc.lisp2str(db))
        except Exception, e:
            print >>sys.stderr, e
            print >>sys.stderr, 'error on %r' % line
            continue
