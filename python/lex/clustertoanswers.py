import sys
from collections import defaultdict

cluster_file = sys.argv[1]
cluster_labels = sys.argv[2]

clusters = defaultdict(list)
for line in open(cluster_file):
    id, q = line.strip().split('\t')
    clusters[int(id)].append(q)

labels = defaultdict(dict)
for line in open(cluster_labels):
    label, cluster, answer = line.strip().split('\t')
    label = int(label)
    cluster = int(cluster)
    labels[cluster][answer] = label

for id in clusters:
    for q in clusters[id]:
        for answer in labels[id]:
            print '%s\t%s\t%s' % (labels[id][answer], q, answer)
