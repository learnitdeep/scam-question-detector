import sys
import lex.lambdacalcutil as lcu
import lex.vocab
import lex.semantics
import qa.lambdacalc as lc

if __name__ == '__main__':
    db_vocab_inv = lex.vocab.read_vocab_inv(open(sys.argv[1]))
    for line in sys.stdin:
        try:
            nl, rest = line.strip().split('\t', 1)
            rest = rest.split('\t')
            for x in rest:
                x = x.strip()
                db = lcu.sem2lisp(lex.semantics.str2obj(x), db_vocab_inv)
                print '%s\t%s' %(nl, lc.lisp2str(db))
        except Exception, e:
            print >>sys.stderr, e
            print >>sys.stderr, 'error on %r' % line
            continue
