import sys
import lex.lambdacalcutil as lcu
import lex.vocab
import qa.lambdacalc as lc

def weight2str(line, nlv, dbv):
    fields = list(int(x) for x in line.split(' '))
    nl = nlv[fields.pop(0)]
    db = lcu.sem2lisp(tuple(fields), dbv)
    return nl, db

if __name__ == '__main__':
    nl_vocab_inv = lex.vocab.read_vocab_inv(open(sys.argv[1]))
    print >>sys.stderr, nl_vocab_inv.items()[0]
    db_vocab_inv = lex.vocab.read_vocab_inv(open(sys.argv[2]))
    for line in sys.stdin:
        try:
            score, rest = line.split(' ', 1)
            score = float(score)
            nl, db = weight2str(rest, nl_vocab_inv, db_vocab_inv)
            print '%0.2f\t%s\t%s' % (score, nl, lc.lisp2str(db))
        except:
            continue
