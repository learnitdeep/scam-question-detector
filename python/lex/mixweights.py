import lex.learn as learn
import sys
weights = learn.mix_weights(sys.argv[1:])
learn.save_weights(sys.stdout, weights)
