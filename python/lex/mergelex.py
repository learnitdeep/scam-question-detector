from collections import defaultdict
import sys
import lex.vocab
import lex.lexicon
path1 = sys.argv[1]
path2 = sys.argv[2]
output = sys.argv[3]

vocab1 = lex.vocab.read_vocab(open(path1 + '/vocab.txt'))
vocab2 = lex.vocab.read_vocab(open(path2 + '/vocab.txt'))
lexicon1 = lex.lexicon.read_lexicon(open(path1+'/lexicon.txt'))
lexicon2 = lex.lexicon.read_lexicon(open(path2+'/lexicon.txt'))

n = max(vocab1.itervalues()) + 1
remap = dict()
for nl in vocab2:
    if nl not in vocab1:
        vocab1[nl] = n
        n += 1
    remap[vocab2[nl]] = vocab1[nl]

vout = open(output + '/vocab.txt', 'w')
for nl in vocab1:
    print >>vout, '%s %s' % (vocab1[nl], nl)
vout.close()

newlex = defaultdict(set)
for i in lexicon1:
    newlex[i].update(lexicon1[i])
for i in lexicon2:
    newlex[remap[i]].update(lexicon2[i])


lout = open(output +'/lexicon.txt', 'w')
for i in newlex:
    for k in newlex[i]:
        print >>lout, '%s %s' % (i, ' '.join(str(x) for x in k))
lout.close()
