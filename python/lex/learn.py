import time
import sys
from lex.parse import parse
from lex.parse import parse_fast
from lex.parse import score_parse
from lex.semantics import str2obj

def example_iter(path):
    f = open(path)
    for line in f:
        try:
            yield read_example(line)
        except:
            continue

def read_example(line):
    fields = line.split('\t')
    s = fields.pop(0)
    sent = dict(lemma=s.split(' '))
    labels = [str2obj(f) for f in fields]
    return sent, labels

def log(*a):
    print >>sys.stderr, ' '.join(str(s) for s in a)

def load_weights(input):
    weights = dict()
    for line in input:
        weight,f = line.strip().split(' ', 1)
        try:
            weight = float(weight)
            if abs(weight) > 1e-8: weights[f] = weight
        except:
            continue
    return weights

def mix_weights(paths):
    n = float(len(paths))
    weights = dict()
    for p in paths:
        for line in open(p):
            weight,f = line.strip().split(' ', 1)
            weight = float(weight)
            weights[f] = weights.get(f, 0.0) + weight/n
    return weights

def save_weights(output, weights):
    for f in weights:
        if abs(weights[f]) > 1e-4:
            print >>output, '%s %s' % (weights[f], f)

def predict(question, nl_vocab, lexicon, weights, z=None):
    best_parse, best_score = None, float('-inf')
    if z is not None:
        if type(z) == list or type(z) == set:
            test = lambda x: x in z
        else:
            test = lambda x: x == z
    for p in parse_fast(question, nl_vocab, lexicon, weights):
        s = score_parse(p, weights)
        m = p.meaning
        if s > best_score and (z is None or test(m)):
            best_parse = p
            best_score = s
    if best_parse:
        return best_parse
    else:
        return None

def perceptron_epoch(examples, nl_vocab, lexicon, weights):
    correct = 0.0
    total = 0.0
    for i, (q, labels) in enumerate(examples):
        total += 1
        ex_start = time.time()
        correct += perceptron_step(q, labels, nl_vocab, lexicon, weights)
        ex_time = time.time() - ex_start
        log('rate', correct/total)
        log('time', ex_time)

def perceptron_step(question, labels, nl_vocab, lexicon, weights):

    qs = ' '.join(question['lemma'])

    log('*'*80)
    log('new example')
    log('q =', qs)
    for l in labels:
        log('label =', l)

    parse = predict(question, nl_vocab, lexicon, weights)

    if parse is None:
        log('could not parse question', repr(qs))
        return 0

    if parse.meaning in labels:
        log('correct', parse.meaning)
        return 1

    log('incorrect')
    log('parse(q) = ', parse.meaning)

    best_parse = predict(question, nl_vocab, lexicon, weights, labels)

    if not best_parse:
        log('could not parse to any labels, not updating')
        return 0

    elif parse.meaning != best_parse.meaning:
        log('updating')
        update_weights(weights, best_parse, parse)
        return 0
    else:
        log('correct')
        return 0

def update_weights(weights, correct, incorrect):
    log('updating weights')
    if correct is None:
        log('could not update weights, correct is None')
        return
    if incorrect is None:
        log('could not update weights, incorrect is None')
        return
    increment_weights(weights, correct.features)
    decrement_weights(weights, incorrect.features)

def increment_weights(weights, features):
    for f in features:
        log(f, ' += 1')
        weights[f] = weights.get(f, 0.0) + 1.0

def decrement_weights(weights, features):
    for f in features:
        log(f, ' -= 1')
        weights[f] = weights.get(f, 0.0) - 1.0
