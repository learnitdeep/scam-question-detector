import sys
from lex.parse import parse
from lex.parse import parse_fast
from lex.parse import score_parse
from lex.parse import generate_patterns_single
from lex.parse import generate_patterns_pair
from itertools import chain
import lex.semantics as sem
def str2align(s):
    chunks = s.strip().split(' ')
    align = []
    for chunk in chunks:
        a, b = chunk.split('-')
        a = int(a)
        b = int(b)
        align.append((a,b))
    return align

def read_alignment_line(line):
    x, y, s = line.split('\t', 2)
    x = x.split(' ')
    y = y.split(' ')
    a = str2align(s)
    return dict(lemma=x), dict(lemma=y), a

def valid_align_subset(old, new, alignment):
    for i,j in alignment:
        if j in new and i not in old: return False
    return True

def paralex_single(parse, q, alignment):
    rpat_q1, ypat_q1 = parse.strings
    rmean_q1, ymean_q1 = parse.concepts
    for rpat_q2, ypat_q2 in generate_patterns_single(q):
        if valid_align_subset(ypat_q1.indexes, ypat_q2.indexes, alignment):
            yield (ypat_q2, ymean_q1)
            yield (rpat_q2, rmean_q1)

def paralex_pair(parse, q, alignment):
    qpat_q1, rpat_q1, ypat_q1 = parse.strings
    qmean_q1, rmean_q1, ymean_q1 = parse.concepts
    for qpat_q2, rpat_q2, ypat_q2 in generate_patterns_pair(q):
        if valid_align_subset(ypat_q1.indexes, ypat_q2.indexes, alignment) \
            and valid_align_subset(rpat_q1.indexes, rpat_q2.indexes, alignment):
            yield (ypat_q2, ymean_q1)
            yield (rpat_q2, rmean_q1)
            yield (qpat_q2, qmean_q1)

def paralex(q1, q2, alignment, nl_vocab, lexicon):
    result = []
    for p in parse(q1, nl_vocab, lexicon):
        if len(p.concepts) == 2:
            result = chain(result, paralex_single(p, q2, alignment))
        else:
            result = chain(result, paralex_pair(p, q2, alignment))
    return result

def paralex_best(q1, q2, alignment, nl_vocab, lexicon, weights):
    best_parse = None
    best_score = float('-inf')
    for p in parse_fast(q1, nl_vocab, lexicon, weights):
        s = score_parse(p, weights)
        if s > best_score:
            best_parse = p
            best_score = s
    if not best_parse: return []
    result = []
    if len(best_parse.concepts) == 2:
        return paralex_single(best_parse, q2, alignment)
    else:
        return paralex_pair(best_parse, q2, alignment)

def read_alignment_line(line):                                                  
    x, y, s = line.split('\t', 2)                                               
    x = x.split(' ')                                                            
    y = y.split(' ')                                                            
    a = str2align(s)                                                            
    return dict(lemma=x), dict(lemma=y), a

def paralex_input(input, nl_vocab, lexicon, maxlen=15, maxnllen=10):
    import time
    times = []
    for i, line in enumerate(input):
        if i % 100000 == 0: print >>sys.stderr, i
        try:
            t0 = time.time()
            q1, q2, alignment = read_alignment_line(line)
            if len(q1['lemma']) > maxlen: continue
            if len(q2['lemma']) > maxlen: continue
            c = 0
            for string, meaning in paralex(q1, q2, alignment, nl_vocab, lexicon):
                if len(string.indexes) > maxnllen: continue
                c += 1
                yield string.string, meaning
            times.append( (time.time()-t0, c, line.strip()))
        except:
            print >>sys.stderr, 'Could not align %r' % line
            continue
    times.sort()
    for t,c,s in times:
        if c: print '%0.8f\t%s\t%s' % (t, c, s)

def paralex_input_best(input, nl_vocab, lexicon, weights, maxlen=15, maxnllen=10):
    for i, line in enumerate(input):
        if i % 100000 == 0: print >>sys.stderr, i
        try:
            q1, q2, alignment = read_alignment_line(line)
            if len(q1['lemma']) > maxlen: continue
            if len(q2['lemma']) > maxlen: continue
            c = 0
            sent = ' '.join(q2['lemma'])
            for string, meaning in paralex_best(q1, q2, alignment, nl_vocab, lexicon, weights):
                if len(string.indexes) > maxnllen: continue
                c += 1
                yield sent, string.string, meaning
        except:
            print >>sys.stderr, 'Could not align %r' % line
            continue
