import sqlite3
from lex.semantics import arg_order_of
from lex.semantics import ORDER_12
from lex.semantics import ORDER_21
from lex.semantics import rel1_ent_dbid_of
from lex.semantics import rel1_rel_dbid_of
from lex.semantics import new_ent

def open_db(path):                                                              
    return sqlite3.connect(path)

def query2sql(q, db_vocab, db_vocab_inv):
    order = arg_order_of(q)
    rel = db_vocab_inv[rel1_rel_dbid_of(q)]
    const = db_vocab_inv[rel1_ent_dbid_of(q)]
    scol = 'arg1' if order == ORDER_21 else 'arg2'
    ccol = 'arg2' if order == ORDER_21 else 'arg1'
    sql = 'SELECT %s FROM tuples WHERE rel=? AND %s=?' % (scol, ccol)
    return sql, (rel, const)

def query2str(q, db_vocab_inv):
    order = arg_order_of(q)
    rel = db_vocab_inv[rel1_rel_dbid_of(q)]
    const = db_vocab_inv[rel1_ent_dbid_of(q)]
    s = '%s(?, %s)' if order == ORDER_21 else '%s(%s, ?)'
    s = s % (rel, const)
    return s        

def query(q, db, db_vocab, db_vocab_inv):
    sql, args = query2sql(q, db_vocab, db_vocab_inv)
    c = db.cursor()
    return sorted(list(set(new_ent(db_vocab[x[0]]) for x in c.execute(sql, args))))
