import lex.semantics as semantics
import sys
import qa.lambdacalc as lc

def unintern_lexitem(item, nl_vocab_inv, db_vocab_inv):
    nlid = item[0]
    dbid = item[1:]
    nl = nl_vocab_inv[nlid]
    db = sem2lisp(dbid, db_vocab_inv)
    return (nl, db)

def intern_lexicon(input, nl_vocab, db_vocab, ignorerrors=True):
    for line in input:
        try:
            nl, db = line.split('\t', 1)
            nlid = nl_vocab[nl]
            db = lc.str2lisp(db)
            sem = lisp2semantics(db, db_vocab)
            yield (nlid,) + sem
        except:
            print >>sys.stderr, 'Could not intern %r' % line
            continue

def lambda_count(expr):
    if not lc.is_lambda(expr):
        return 0
    else:
        return 1 + lambda_count(lc.lambda_body(expr))

def lisp2semantics(expr, vocab):
    c = lambda_count(expr)
    if c == 0:
        return entity2obj(expr, vocab)
    elif c == 1:
        return rel12obj(expr, vocab)
    elif c == 2:
        return rel2obj(expr, vocab)
    elif c == 3:
        return abstr2obj(expr, vocab)
    else:
        raise ValueError('could not convert %s' % lc.lisp2str(expr))

def abstr2obj(expr, vocab):
    expr = lc.lambda_body(expr)
    firstvar = lc.formal_var(expr)
    secondvar = lc.formal_var(lc.lambda_body(expr))
    body = lc.lambda_body(lc.lambda_body(expr))
    order = None
    if body[1] == firstvar:
        order = semantics.ORDER_12
    elif body[1] == secondvar:
        order = semantics.ORDER_21
    else:
        raise ValueError('could not convert %s' % lc.lisp2str(expr))
    return semantics.new_abstr(order)

def rel12obj(expr, vocab):
    var = lc.formal_var(expr)
    rel, arg1, arg2 = lc.lambda_body(expr)
    const = None
    order = None
    if arg1 == var:
        const = arg2
        order = semantics.ORDER_21
    else:
        const = arg1
        order = semantics.ORDER_12
    rel_id = vocab[rel]
    const_id = vocab[const]
    return semantics.new_rel1(order, rel_id, const_id)

def rel2obj(expr, vocab):
    firstvar = lc.formal_var(expr)
    secondvar = lc.formal_var(lc.lambda_body(expr))
    body = lc.lambda_body(lc.lambda_body(expr))
    order = None
    if body[1] == firstvar :
        order = semantics.ORDER_12
    elif body[1] == secondvar:
        order = semantics.ORDER_21
    else:
        raise ValueError('could not convert %s' % lc.lisp2str(expr))
    id = vocab[body[0]]
    return semantics.new_rel2(order, id)

def entity2obj(expr, vocab):
    id = vocab[expr]
    return semantics.new_ent(id)

abstract21 = lc.str2lisp('(lambda $r (lambda $y (lambda $x (($r $y) $x))))')
abstract12 = lc.str2lisp('(lambda $r (lambda $y (lambda $x (($r $x) $y))))')

def sem2str(sem, vocab):
    t = semantics.type_of(sem)
    if t == semantics.ENT:
        return vocab[semantics.ent_dbid_of(sem)]
    elif t == semantics.ABSTR:
        o = semantics.arg_order_of(sem)
        if o == semantics.ORDER_12:
            return 'r(y, ?)'
        else:
            return 'r(?, y)'
    elif t == semantics.REL1:
        const = vocab[semantics.rel1_ent_dbid_of(sem)]
        rel = vocab[semantics.rel1_rel_dbid_of(sem)]
        o = semantics.arg_order_of(sem)
        if o == semantics.ORDER_12:
            return '%s(%s, ?)' % (rel, const)
        else:
            return '%s(?, %s)' % (rel, const)
    else:
        o = semantics.arg_order_of(sem)
        id = semantics.rel2_dbid_of(sem)
        rel = vocab[id]
        if o == semantics.ORDER_12:
            return '%s(y, ?)' % rel
        elif o == semantics.ORDER_21:
            return '%s(?, y)' % rel

def sem2lisp(sem, vocab):
    t = semantics.type_of(sem)
    if t == semantics.ENT:
        return vocab[semantics.ent_dbid_of(sem)]
    elif t == semantics.ABSTR:
        o = semantics.arg_order_of(sem)
        if o == semantics.ORDER_12:
            return abstract12
        elif o == semantics.ORDER_21:
            return abstract21
        else:
            raise ValueError('Unknown order: %s' % str(sem))
    elif t == semantics.REL1:
        const = vocab[semantics.rel1_ent_dbid_of(sem)]
        rel = vocab[semantics.rel1_rel_dbid_of(sem)]
        o = semantics.arg_order_of(sem)
        if o == semantics.ORDER_12:
            return ('lambda', '$x', (rel, const, '$x'))
        elif o == semantics.ORDER_21:
            return ('lambda', '$x', (rel, '$x', const))
        else:
            raise ValueError('Unknown order: %s' % str(sem))
    elif t == semantics.REL2:
        o = semantics.arg_order_of(sem)
        id = semantics.rel2_dbid_of(sem)
        rel = vocab[id]
        if o == semantics.ORDER_12:
            return ('lambda', '$y', ('lambda', '$x', (rel, '$y', '$x')))
        elif o == semantics.ORDER_21:
            return ('lambda', '$y', ('lambda', '$x', (rel, '$x', '$y')))
        else:
            raise ValueError('Unknown order: %s' % str(sem))
    else:
        raise ValueError('unknown type: %s' % str(sem))

def convert_labeled(input, db_vocab):
    for line in input:
        fields = line.split('\t')
        sent = fields.pop(0)
        labeled = []
        for f in fields:
            try:
                labeled.append(lisp2semantics(lc.str2lisp(f), db_vocab))
            except:
                continue
        if labeled: yield sent, labeled
