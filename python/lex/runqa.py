import time
import sys
import lex.lexicon
import lex.vocab
import lex.db
import lex.learn
import qa.server
import lex.parse
import lex.semantics
from collections import namedtuple
from collections import defaultdict

Context = namedtuple('Context', 'lexicon nl_vocab db_vocab db_vocab_inv db weights')

config = dict()
config['nlp_host'] = 'localhost'
config['nlp_port'] = 8082

def get_sent(s):
    return qa.server.get_sent(s, **config)

def log(*a):
    print >>sys.stderr, ' '.join(str(x) for x in a)

def init_context():
    lexicon_root, weights_path, database_root = sys.argv[1:]
    log('loading lexicon')
    lexicon = lex.lexicon.read_lexicon(open('%s/lexicon.txt' % lexicon_root))
    log('loading vocabs')
    nl_vocab = lex.vocab.read_vocab(open('%s/vocab.txt' % lexicon_root))
    db_vocab = lex.vocab.read_vocab(open('%s/vocab.txt' % database_root))
    db_vocab_inv = lex.vocab.read_vocab_inv(open('%s/vocab.txt' % database_root))
    log('loading weights')
    weights = lex.learn.load_weights(open(weights_path))
    log('loading database')
    db = lex.db.open_db('%s/tuples.db' % database_root)   
    return Context(lexicon, nl_vocab, db_vocab, db_vocab_inv, db, weights)

def run_on_question(s, con):
    q = get_sent(s)
    scored_answers = defaultdict(lambda: float('-inf'))
    scored_queries = lex.parse.get_scored_queries(q, con.nl_vocab, con.lexicon, con.weights)
    i = 0
    for score, query in sorted(scored_queries, reverse=True):
        if i >= 100: break
        answers = lex.db.query(query, con.db, con.db_vocab, con.db_vocab_inv)
        for a in answers:
            full_answer = lex.semantics.apply(query, a)
            sa = (
                con.db_vocab_inv[lex.semantics.rel0_rel_of(full_answer)],
                con.db_vocab_inv[lex.semantics.rel0_arg1_of(full_answer)],
                con.db_vocab_inv[lex.semantics.rel0_arg2_of(full_answer)],
            )
            scored_answers[sa] = max(scored_answers[sa], score)
        i += 1
    return scored_answers

if __name__ == '__main__':
    context = init_context()
    for q in sys.stdin:
        q = q.strip()
        log(q)
        t0 = time.time()
        try:
            scored_answers = run_on_question(q, context)
            for a in scored_answers:
                print '%s\t%0.2f\t%s' % (q, scored_answers[a], ' '.join(a))
            log(time.time()-t0)
        except Exception, e:
            print >>sys.stderr, e
            print >>sys.stderr, 'error: %r' % q
