import sys
from lex.eval import load_labels
from collections import defaultdict

labels = load_labels(open(sys.argv[1]))

results = defaultdict(list)
for line in sys.stdin:
    x,conf,ans = line.rstrip("\n").split('\t')
    ans = tuple(ans.split(' '))
    conf = float(conf)
    results[x].append((conf,ans))

for x in results:
    c,a = max(results[x])
    print '%s\t%s\t%0.8f\t%s' % (x, ' '.join(a), c, labels[x][a])
