#!/bin/bash
set -u
set -e
if [ $# -lt 3 ]; then
    echo "Usage: run_aveweights.sh input1 input2 ... inputN output"
    echo "Where"
    echo "    input1, ..., inputN = weight files"
    echo "    output = output weight file"
    exit 1
fi
length=$(($#-1))
array=${@:1:$length}
last=${@:$#}
echo $array
echo $last

PYTHONPATH=$PWD/python python -m lex.mixweights $array > $last
