#!/bin/bash
set -u
set -e
if [ $# != 5 ]; then
    echo "Usage: run_lexlearn.sh paraphrases initlex initweights outlex outlabs"
    echo "Where"
    echo "    paraphrases = word-aligned paraphrase corpus file"
    echo "    initlex = initial lexicon directory"
    echo "    initweights = initial weights"
    echo "    outlex = the output learned lexicon directory"
    echo "    outlabs = the output labeled (question, query) pairs"
    exit 1
fi
paraphrases=$1
initlex=$2
initweights=$3
outlex=$4
outlabs=$5

sortcmd="sort -S 500M --parallel=8"

if [ ! -f $outlex ]; then
    mkdir $outlex
fi

outlextmp=$outlex/tmp
if [ ! -f $outlextmp ]; then
    mkdir $outlextmp
fi

pats=$outlextmp/pats
freqpats=$outlextmp/freqpats

# Run the paralex lexical learning algorithm
echo "Running lexicon learner"
PYTHONPATH=$PWD/python python -m lex.paralex $initlex $initweights $pats $outlabs < $paraphrases

# Get the patterns that appear more than once
echo "Filtering out low-frequency patterns"
cut -f2- $pats | $sortcmd | uniq -c | sed 's/^ *\([0-9]*\) /\1	/g' | \
    grep -v '^1	' | cut -f2- > $freqpats

# Create a new vocab file
echo "Creating vocab file"
vocab=$outlextmp/vocab.txt
cut -f1 $freqpats | $sortcmd | uniq | nl | sed 's/^ *//g' > $vocab

# Create the lexicon file
echo "Creating lexicon file"
lex=$outlextmp/lexicon.txt
PYTHONPATH=$PWD/python python -m lex.createlex $vocab > $lex < $freqpats

# Merge learned lexicon with input one
echo "Merging with initial lexicon"
PYTHONPATH=$PWD/python python -m lex.mergelex $outlextmp $initlex $outlex
