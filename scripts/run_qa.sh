#!/bin/bash
set -u
set -e
if [ $# != 4 ]; then
    echo "Usage: run.sh questions lexicon weights db"
    echo "Where"
    echo "    questions = file with one question per line"
    echo "    lexicon = path to lexicon dir e.g. data/lexicons/paralex"
    echo "    weights = path to weights file e.g. data/weights/paralex.txt"
    echo "    db = path to database e.g. data/database"
    echo 
    echo "Runs the QA system on each of the given questions, returning the"
    echo "answers for each question. Writes output records to stdout, where "
    echo "each record is tab-separated with fields:"
    echo "    question"
    echo "    answer confidence"
    echo "    tuple answer"
    echo
    echo "Make sure to start the NLP process script by running "
    echo "    ./scripts/start_nlp.sh "
    exit 1
fi
questions=$1
lexicon=$2
weights=$3
db=$4
PYTHONPATH=$PWD/python python -m lex.runqa $lexicon $weights $db < $questions
