#!/bin/bash
set -u
set -e
if [ $# != 4 ]; then
    echo "Usage: run_weightlearn.sh lexicon initweight data output"
    echo "Where"
    echo "    lexicon = a lexicon directory (e.g. data/lexicons/paralex)"
    echo "    initweight = initial weights (e.g. data/weights/zero.txt)"
    echo "    data = labeled data file"
    echo "    output = output weights file"
    echo
    echo "Runs a single perceptron iteration on the given data and writes the"
    echo "final weight vector to output."
    exit 1
fi

lexicon=$1
initweight=$2
data=$3
output=$4

PYTHONPATH=$PWD/python python -m lex.perceptroniter $lexicon $initweight $data $output
