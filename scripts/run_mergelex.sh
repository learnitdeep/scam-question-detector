#!/bin/bash
set -u
set -e
if [ $# != 3 ]; then
    echo "Usage: run_mergelex.sh lex1 lex2 outlex"
    echo "Where"
    echo "    lex1 = a lexicon"
    echo "    lex2 = another lexicon"
    echo "    outlex = lex1 and lex2 merged together"
    exit 1
fi

lex1=$1
lex2=$2
outlex=$3

PYTHONPATH=$PWD/python python -m lex.mergelex $lex1 $lex2 $outlex
