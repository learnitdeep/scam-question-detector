#!/bin/bash
set -u
set -e
cd external/stanford-corenlp-python
python corenlp.py -H 0.0.0.0 -p 8082
